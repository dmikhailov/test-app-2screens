var express = require('express');
var path = require('path');

var app = express(),
    port = 8080;

app.use(express.static('client/build'));
app.use('/vendors', express.static('client/vendors'));
app.use('/data', express.static('server/data'));

app.use(function (req, res, next) {
    if (req.url.indexOf('/data') == 0) {
        res.status(404);
        res.send({error: 'Not found'});
    } else {
        res.sendFile(path.resolve('client/build/index.html'));
    }
});

app.listen(port, function() {
    console.log('server started at http://localhost:' + port);
});

