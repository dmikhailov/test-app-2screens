#here is a description of the test task which you need to develop

please create a single page app (using AngularJS) which would have 2 screens (tabs):
1st screen would display list of field names (with some basic filtering by the name) and allow user to select which fields he wants to see on the 2nd screen (basic checkbox list with field names and some input on top of it to filter the list by name)
when user will type in the filter input - list should be re-rendered (updated) and only fields matching by name (contains) should be displayed
for example if you have fieldA, fieldB, fieldC displayed and user enters filter A then you display only fieldA

2nd screen would display those selected fields (single column layout is fine), where user would be able to enter some data (so those fields need to be data bound)

please create some basic metadata (in-memory / JSON) which would describe list of available fields (to be displayed on the 1st screen)
please use different types of controls (types of fields), like basic text fields (inputs), date pickers, money inputs, numeric inputs, drop downs

again, this metadata should be used to display list of available fields (1st screen)
and then also used on the 2nd screen to render those controls
you may have also some validation (required / min / max value / min / max length) implemented on the 2nd screen

so 2nd screen needs to render dynamically controls, based on user selection on the 1st screen

please make sure both screens are bookmarkable and whatever user has entered into user inputs - remains there (if user just switch between tabs)
(in other words make sure UI views state is preserved when user switches between them)

I'd like to see responsive markup as well for this page

you can use any router you want (ng-router or ui-router) - up to you
I personally prefer ui-router though

#start
### install node modules
npm install

### run server
gulp