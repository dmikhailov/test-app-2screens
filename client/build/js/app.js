(function() {
    'use strict';

    angular.
        module('app', ['ui.router', 'templates', 'ngMaterial', 'ngAnimate', 'ngMessages']);
})();

(function () {
    'use strict';

    angular
        .module('app')
        .config(function ($locationProvider, $httpProvider) {
            $locationProvider.html5Mode().enabled = true;
        });
})();

(function() {
    'use strict';

    angular
        .module('app')
        .config(function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider
                .otherwise('/error/404');

            $stateProvider
                .state('404', {
                    url: '/error/404',
                    templateUrl: 'errors/page404.html'
                })
                .state('screen1', {
                    url: '/',
                    text: 'Screen1',
                    templateUrl: 'screen1.html',
                    controller: 'Screen1Controller'
                })
                .state('screen2', {
                    url: '/screen2',
                    text: 'Screen2',
                    templateUrl: 'screen2.html',
                    controller: 'Screen2Controller'
                });
        });

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('Screen1Controller', function ($scope, $state, dataService, storageService) {
            if (storageService.isSaved()) {
                $scope.data = storageService.get();
                return;
            }

            dataService.getData().then(function (response) {
                storageService.save(response.data);
            }).finally(function() {
                $scope.data = storageService.get();
            });
        });
})();
(function () {
    'use strict';

    angular
        .module('app')
        .controller('Screen2Controller', function ($scope, $state, dataService, storageService) {
            if (storageService.isSaved()) {
                $scope.data = storageService.get();
                return;
            }

            dataService.getData().then(function (response) {
                storageService.save(response.data);
            }).finally(function() {
                $scope.data = storageService.get();
            });
        });
})();
(function () {
    'use strict';

    angular
        .module('app')
        .controller('TabController', function ($scope, $state) {
            $scope.$state = $state;
        });
})();
(function () {
    'use strict';

    angular
        .module('app')
        .service('dataService', function ($http) {
            var dataPromise = $http({
                method: 'GET',
                url: 'data/field-list.json'
            });
            return {
                getData: function() {
                    return dataPromise;
                }
            };
        });
})();
(function () {
    'use strict';

    angular
        .module('app')
        .service('storageService', function ($http) {
            var data = [],
                saved = false;
            return {
                save: function(iData) {
                    data = iData;
                    saved = true;
                },
                isSaved: function() {
                    return saved;
                },
                get: function() {
                    return data;
                }
            };
        });
})();
angular.module("templates", []).run(["$templateCache", function($templateCache) {$templateCache.put("screen1.html","\n<md-content>\n  <md-input-container>\n    <label>Filter</label>\n    <input type=\"text\" ng-model=\"searchText\"/>\n  </md-input-container>\n  <md-checkbox ng-repeat=\"item in data | filter:searchText\" aria-label=\"{{item.title}}\" ng-model=\"item.isEnabled\">{{item.title}}</md-checkbox>\n</md-content>");
$templateCache.put("screen2.html","\n<md-content>\n  <form name=\"s2Form\">\n    <md-input-container ng-repeat=\"item in data\">\n      <div ng-if=\"item.isEnabled\">\n        <label>{{item.title}}</label>\n        <div ng-switch=\"item.type\">\n          <input ng-switch-when=\"input\" name=\"{{item.name}}\" type=\"text\" ng-model=\"item.value\" ng-required=\"{{item.required}}\"/>\n          <input ng-switch-when=\"password\" name=\"{{item.name}}\" type=\"password\" ng-model=\"item.value\" ng-required=\"{{item.required}}\"/>\n          <input ng-switch-when=\"number\" name=\"{{item.name}}\" type=\"number\" ng-model=\"item.value\" min=\"1\" max=\"100\" ng-required=\"{{item.required}}\"/>\n          <md-select ng-switch-when=\"select\" name=\"{{item.name}}\" ng-model=\"item.value\" ng-required=\"{{item.required}}\">\n            <md-option ng-value=\"0\">Female</md-option>\n            <md-option ng-value=\"1\">Male</md-option>\n          </md-select>\n          <md-datepicker ng-switch-when=\"datepicker\" name=\"{{item.name}}\" ng-model=\"item.value\" ng-required=\"{{item.required}}\"></md-datepicker>\n          <div ng-switch-when=\"checkbox\">\n            <md-checkbox ng-model=\"item.value\" name=\"{{item.name}}\" ng-required=\"{{item.required}}\"></md-checkbox>\n          </div>\n        </div>\n      </div>\n    </md-input-container>\n  </form>\n</md-content>");
$templateCache.put("errors/page404.html","\n<h1>404 Not found</h1>");}]);