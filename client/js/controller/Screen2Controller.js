(function () {
    'use strict';

    angular
        .module('app')
        .controller('Screen2Controller', function ($scope, $state, dataService, storageService) {
            if (storageService.isSaved()) {
                $scope.data = storageService.get();
                return;
            }

            dataService.getData().then(function (response) {
                storageService.save(response.data);
            }).finally(function() {
                $scope.data = storageService.get();
            });
        });
})();