(function () {
    'use strict';

    angular
        .module('app')
        .controller('TabController', function ($scope, $state) {
            $scope.$state = $state;
        });
})();