(function() {
    'use strict';

    angular.
        module('app', ['ui.router', 'templates', 'ngMaterial', 'ngAnimate', 'ngMessages']);
})();
