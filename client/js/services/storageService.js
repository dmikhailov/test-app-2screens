(function () {
    'use strict';

    angular
        .module('app')
        .service('storageService', function ($http) {
            var data = [],
                saved = false;
            return {
                save: function(iData) {
                    data = iData;
                    saved = true;
                },
                isSaved: function() {
                    return saved;
                },
                get: function() {
                    return data;
                }
            };
        });
})();