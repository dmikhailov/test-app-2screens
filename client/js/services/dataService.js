(function () {
    'use strict';

    angular
        .module('app')
        .service('dataService', function ($http) {
            var dataPromise = $http({
                method: 'GET',
                url: 'data/field-list.json'
            });
            return {
                getData: function() {
                    return dataPromise;
                }
            };
        });
})();