(function () {
    'use strict';

    angular
        .module('app')
        .config(function ($locationProvider, $httpProvider) {
            $locationProvider.html5Mode().enabled = true;
        });
})();
