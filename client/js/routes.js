(function() {
    'use strict';

    angular
        .module('app')
        .config(function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider
                .otherwise('/error/404');

            $stateProvider
                .state('404', {
                    url: '/error/404',
                    templateUrl: 'errors/page404.html'
                })
                .state('screen1', {
                    url: '/',
                    text: 'Screen1',
                    templateUrl: 'screen1.html',
                    controller: 'Screen1Controller'
                })
                .state('screen2', {
                    url: '/screen2',
                    text: 'Screen2',
                    templateUrl: 'screen2.html',
                    controller: 'Screen2Controller'
                });
        });

})();
